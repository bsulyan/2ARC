### SOUND DECLARATION

sound "change" do
  effect do
    duty_cycle :higher
    velocity 15
    envelope_decay :disable
    length 5
    pitch 200
  end
  effect do
    length 4
    pitch 100
  end
end

sound "finish" do
  effect do
    duty_cycle :highest
    velocity 10 
    envelope_decay :disable
    length 20 
    pitch 90
  end
  effect do
    length 15
    pitch 100
  end
  effect do
    pitch 90
  end
end

### MUSIC DECLARATION

music "sarabande" do
  tempo :allegro
  channel "string" do
    d2   :eighth
    rest :eighth
    d2   :eighth
    rest
    d2   :eighth
    
    a1   :eighth
    rest :eighth
    a1   :eighth
    bs1  :eighth
    a1   :eighth
    g1   :eighth
    
    f1   :eighth
    rest :eighth
    f1   :eighth
    rest
    f1   :eighth
    
    c2   :eighth
    rest :eighth
    c2   :eighth
    c2   :eighth
    bs1  :eighth
    a1   :eighth
    
    g1   :eighth
    rest :eighth
    g1   :eighth
    rest 
  end
end

### SPRITE DECLARATION

declare do
  color_flag 0
  score 0
  is_attacking 0
  move 0
  frame 0
  i 0
  block1 1
  block2 1
  block3 1
  block4 1
  block5 1
  block6 1
  block7 1
  block8 1
  block9 1
  block10 1
  initGame 1
  lvl 1
  wintest 1
  start 0
  initstart 0
  life 2


background1 <<-EOH
        
       1
       1
      11
      11
     111
   11111
 1111111
EOH

background2 <<-EOH
        
1       
1       
11      
11      
111     
11111   
1111111 
EOH

background3 <<-EOH
 1111111
   11111
     111
      11
      11
       1
       1
        
EOH

background4 <<-EOH
1111111 
11111   
111     
11      
11      
1       
1       
        
EOH

 side_wall <<-EOH
11122111
11122111
11122111
11122111
11122111
11122111
11122111
11122111
EOH
top_wall <<-EOH
11111111
11111111
11111111
22222222
22222222
11111111
11111111
11111111
EOH
left_corner <<-EOH
11111111
11111111
11111111
11122222
11122222
11122111
11122111
11122111
EOH
right_corner <<-EOH
11111111
11111111
11111111
22222111
22222111
11122111
11122111
11122111
EOH

block <<-EOH
2222222222222211
2222222222222211
2222222222222211
2222222222222211
2222222222222211
2222222222222211
1111111111111111
1111111111111111
EOH

  player <<-EOH
   3111111111111111111111113    
  331222222222222222222222133   
 33312111111111111111111121333  
3333121333333333333333331213333 
3333121333333333333333331213333 
 33312111111111111111111121333  
  331222222222222222222222133   
   3111111111111111111111113    
EOH

  title <<-EOH
  1111  
22222222
22222222
 222222 
 222222 
  2222  
  2222  
   22   
EOH


  ball <<-EOH
         
  222   
 22222  
2222221 
2222211 
 22211  
  211   
         
EOH

end

### SCENE DECLARATION


scene "title" do
  color :bg, :black, :lightest
  
  color :palette_x1, :blue, :lighter
  color :palette_x2, :blue, :lighter
  color :palette_x3, :blue, :lighter
  
  color :palette_y1, :deepred, :lighter
  color :palette_y2, :deepred, :darker
  color :palette_y3, :deepred, :darker
  paint range("0px", "0px", "250px", "250px"), :palette_y

  color :palette_z1, :gray, :lightest
  color :palette_z2, :black
  color :palette_z3, :black
  paint range(0, 11, 30, 12), :palette_z

  screen <<-EOH, { A:"title"}   
                                
  AAAA   A    AAA   AAA  AAAA   
  A     A A  A     A     A      
  A    A   A AAAA  AAAA  AAAA   
  A    AAAAA     A     A A      
  A    A   A A   A A   A A      
  AAAA A   A  AAA   AAA  AAAA   
                                
 AAA  AAA  AAAA  AAA  A   A AAAA
 A  A A  A  AA  A   A A   A A   
 AAA  AAA   AA  A   A A   A AAAA
 A  A AA    AA  A   A A   A A   
 A  A A A   AA  A   A A   A A   
 AAAB A  A AAAA  AAAAA AAA  AAAA
EOH

  label "press start", 10, 20
  play "sarabande"
  
  main_loop <<-EOH
    frame+=1
    if (frame%30)==1 
      if color_flag%2 
        color :text, :white
      else
        color :text, :deepred, :darker
      end
      color_flag+=1
    end
    
    if is_pressed(:start)
      sound "change"
      stop
      frame=0
      fade_out 5
      wait 100
      goto "prep_1st"
    end
  EOH
end



scene "game" do
  color:bg, :blue, :darker
  color:sprite, :black
  play "sarabande"
  
  screen <<-EOH, { A:"side_wall", B:"top_wall", C:"left_corner", D:"right_corner", E:"background1", F:"background2", G:"background3", H:"background4"}                                     
 CBBBBBBBBBBBBBBBBBBBBBBBBBBBBD 
 A                            A 
 A EF    EF    EF    EF    EF A 
 A GH    GH    GH    GH    GH A 
 A                            A 
 A    EF    EF    EF    EF    A 
 A    GH    GH    GH    GH    A 
 A                            A 
 A EF    EF    EF    EF    EF A 
 A GH    GH    GH    GH    GH A 
 A                            A 
 A    EF    EF    EF    EF    A 
 A    GH    GH    GH    GH    A 
 A                            A 
 A                            A 
 A EF    EF    EF    EF    EF A 
 A GH    GH    GH    GH    GH A 
 A                            A 
 A    EF    EF    EF    EF    A 
 A    GH    GH    GH    GH    A 
 A                            A 
 A                            A 
 A EF    EF    EF    EF    EF A 
 A GH    GH    GH    GH    GH A 
 A                            A 
 A    EF    EF    EF    EF    A 
 A    GH    GH    GH    GH    A 
 A                            A 
EOH
  fade_in
  main_loop <<-EOH
    frame += 1
    inline "oam_clear();"
    if initGame == 1
      initGame = 0
      ball.x = 102
      ball.y = 216
      player.x = 90
      initstart = 1
    end
    if start == 1
      if ball.y >= 18           
        if move == 1
          ball.y -= 2
        elsif move == 2
          ball.x -= 2
          ball.y -= 1
        elsif move == 4
          ball.x -= 1
          ball.y -= 2
        elsif move == 4
          ball.x -= 1
          ball.y -= 2
        elsif move == 6
          ball.x += 1
          ball.y -= 2
        elsif move == 8
          ball.x += 2
          ball.y -= 1
        end
      else                    ### COLLISION SOMMET
        if move == 1
          move = 0
        elsif move == 2 
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
      end
      if ball.x <= 16 || ball.x >= 232        ### COLLISION MUR GAUCHE OU DROIT
        if move == 2
          move = 8
        elsif move == 3
          move = 9
        elsif move == 4
          move = 6
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 7
          move = 5
        elsif move == 8
          move = 2
        elsif move == 9
          move = 3
        end
      end
      if ball.y <= 213 
        if move == 0
          ball.y +=2
        elsif move == 3
          ball.y +=1
          ball.x +=2
        elsif move == 5
          ball.y +=2
          ball.x +=1
        elsif move == 5
          ball.y +=2
          ball.x +=1
        elsif move == 7
          ball.y +=2
          ball.x -=1
        elsif move == 9
          ball.y +=1
          ball.x -=2
        end
      else
        if ball.x<=player.x-6 || ball.x>=player.x+30
          if life > 0
            life -=1
            initstart = 1
            ball.x = player.x + 12
            ball.y = 216
            start = 0
          else
            goto "game_over"
          end
        elsif ball.x>=player.x-6 && ball.x<=player.x    ###COLLISION JOUEUR
          move = 2
        elsif ball.x>=player.x+1 && ball.x<=player.x+7    ###COLLISION JOUEUR
          move = 4
        elsif ball.x>=player.x+8 && ball.x<=player.x+16    ###COLLISION JOUEUR
          move = 1
        elsif ball.x>=player.x+17 && ball.x<=player.x+23    ###COLLISION JOUEUR
          move = 6
        elsif ball.x>=player.x+24 && ball.x<=player.x+30    ###COLLISION JOUEUR
          move = 8
        end
      end
    end
    
    sprite "ball"
    player.y = 220
    if is_pressed(:left) && player.x>18
      player.x-=3
      if initstart == 1
        ball.x-=3
      end
    end
    if is_pressed(:right) && player.x<206
       player.x+=3
       if initstart == 1
         ball.x+=3
       end
    end
    sprite "player"
    
    if (frame%5)==1 
      if is_pressed(:start)
        if start == 1
          start = 0
        else
          start = 1
        end
        if initstart == 1
          initstart = 0
        end
      end
    end

    if is_pressed(:a)
      start = 1
      initstart = 0
    end   
    

    if lvl == 2
      color :bg, :red, :darker
    elsif lvl == 3
      color :bg, :black
    end

    if block1 == 1
      if lvl == 1
        block.x = 50
        block.y = 35
      elsif lvl == 2
        block.x = 30
        block.y = 30
      elsif lvl == 3
        block.x = 30
        block.y = 30
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block1 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block1 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block1 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block1 = 0
      end 
    end
  
    if block2 == 1
      if lvl == 1
        block.x = 70
        block.y = 55
      elsif lvl == 2
        block.x = 30
        block.y = 50
      elsif lvl == 3
        block.x = 30
        block.y = 50
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block2 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block2 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block2 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block2 = 0
      end 
    end
  
    if block3 == 1
      if lvl == 1
        block.x = 90
        block.y = 35
      elsif lvl == 2
        block.x = 30
        block.y = 70
      elsif lvl == 3
        block.x = 30
        block.y = 70
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block3 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block3 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block3 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block3 = 0
      end 
    end

    if block4 == 1
      if lvl == 1
        block.x = 110
        block.y = 55
      elsif lvl == 2
        block.x = 30
        block.y = 90
      elsif lvl == 3
        block.x = 130
        block.y = 30
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block4 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block4 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block4 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block4 = 0
      end 
    end

    if block5 == 1
      if lvl == 1
        block.x = 130
        block.y = 35
      elsif lvl == 2
        block.x = 210
        block.y = 30
      elsif lvl == 3
        block.x = 130
        block.y = 50
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block5 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block5 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block5 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block5 = 0
      end 
    end

    if block6 == 1
      if lvl == 1
        block.x = 150
        block.y = 55
      elsif lvl == 2
        block.x = 210
        block.y = 50
      elsif lvl == 3
        block.x = 130
        block.y = 70
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block6 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block6 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block6 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block6 = 0
      end 
    end
    if block7 == 1
      if lvl == 1
        block.x = 170
        block.y = 35
      elsif lvl == 2
        block.x = 210
        block.y = 70
      elsif lvl == 3
        block.x = 210
        block.y = 30
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block7 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block7 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block7 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block7 = 0
      end 
    end

    if block8 == 1
      if lvl == 1
        block.x = 190
        block.y = 55
      elsif lvl == 2
        block.x = 210
        block.y = 90
      elsif lvl == 3
        block.x = 210
        block.y = 50
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block8 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block8 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block8 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block8 = 0
      end 
    end

    if block9 == 1
      if lvl == 1
        block.x = 210
        block.y = 35
      elsif lvl == 2
        block.x = 30
        block.y = 110
      elsif lvl == 3
        block.x = 210
        block.y = 50
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block9 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block9 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block9 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block9 = 0
      end 
    end
    if block10 == 1
      if lvl == 1
        block.x = 30
        block.y = 55
      elsif lvl == 2
        block.x = 210
        block.y = 110
      elsif lvl == 3
        block.x = 210
        block.y = 70
      end
      sprite 'block'
      if ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y + 8) && (ball.y <= block.y + 10))    ###COLLISION BAS DU BLOCK
        if move == 1
          move = 0
        elsif move == 2
          move = 9
        elsif move == 4
          move = 7
        elsif move == 6
          move = 5
        elsif move == 8
          move = 3
        end
        block10 = 0
      elsif ((ball.x >= block.x - 2) && (ball.x <= (block.x + 18)) && (ball.y >= block.y - 2) && (ball.y <= block.y))    ###COLLISION HAUT DU BLOCK
        if move == 0
          move = 1
        elsif move == 3
          move = 8
        elsif move == 5
          move = 6
        elsif move == 7
          move = 4
        elsif move == 9
          move = 2
        end
        block10 = 0
      elsif ((ball.x >= block.x - 3) && (ball.x <= (block.x)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION GAUCHE DU BLOCK
        if move == 3
          move = 9
        elsif move == 5
          move = 7
        elsif move == 6
          move = 4
        elsif move == 8
          move = 2
        end
        block10 = 0
      elsif ((ball.x >= block.x + 16) && (ball.x <= (block.x + 19)) && (ball.y >= block.y) && (ball.y <= block.y + 8))    ###COLLISION DROIT DU BLOCK
        if move == 2
          move = 8
        elsif move == 4
          move = 6
        elsif move == 7
          move = 5
        elsif move == 9
          move = 3
        end
        block10 = 0
      end 
    end
    
    wintest = block1+block2+block3+block4+block5+block6+block7+block8+block9+block10
    if lvl < 4
      if wintest == 0
        lvl+=1
        block1 = 1
        block2 = 1
        block3 = 1
        block4 = 1
        block5 = 1
        block6 = 1
        block7 = 1
        block8 = 1
        block9 = 1
        block10 = 1
        initstart = 1
        ball.x = player.x + 12
        ball.y = 216
        start = 0
        if lvl == 2
          fade_out
          goto "prep_2nd"
        elsif lvl == 3
          fade_out
          goto "prep_3rd"
        end
  
      end
    else
      goto "win"
    end
  EOH
  end

scene "prep_1st" do
  color :bg, :black
  color :text, :white
  label "1st stage", 12, 13
  fade_in
  wait 600
  fade_out 5
  goto "game"
end

scene "prep_2nd" do
  color :bg, :black
  color :text, :white
  label "2nd stage", 12, 13
  fade_in
  wait 600
  fade_out 5
  goto "game"
end

scene "prep_3rd" do
  color :bg, :black
  color :text, :white
  label "3rd stage", 12, 13
  fade_in
  wait 600
  fade_out 5
  goto "game"
end

scene "game_over" do
  color :bg, :blue
  color :text, :white
  label "You loose", 10, 15
  sound "finish"
end

scene "win" do
  color :bg, :blue
  color :text, :white
  label "You win", 10, 15
  sound "finish"
  
end